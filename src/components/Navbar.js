import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

const NavBar = () => {
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <a className="navbar-brand pl-5 text-white font-weight-bold">Todo-Todo</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <a className="nav-item nav-link active" href="#" style={{paddingLeft: 850}}>My Todos<span className="sr-only">(current)</span></a>
                    <a className="nav-item nav-link navbar-text" href="#">Logout</a>
                    <a className="nav-item nav-link" href="#">Donate Developer</a>
                </div>
            </div>
        </nav>
    );
}

export default NavBar;