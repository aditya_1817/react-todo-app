import React from 'react';

const TodoItem = ({todo}) => {
    return(

        <div className="todoItem">
           <div className="row"style={{width:590}}>
               <div className="col-md-2 d-flex align-items-center border-bottom">
                   <p>{todo.id}</p>
               </div>
               <div className="col-md-8 pt-2 border-bottom">
                   <h1>{todo.message}</h1>
               </div>
               <div className="col-md-2 d-flex align-items-center border-bottom">
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                            <label className="form-check-label" htmlFor="exampleCheck1">Done</label>
                    </div>
               </div>
           </div>
        </div>
    )
};

export default TodoItem;
