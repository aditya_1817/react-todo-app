import React from 'react';

const Preloader=()=>{
    return <h1 className="preloader">loading...</h1>
}

export default Preloader;