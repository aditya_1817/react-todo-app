import React, {useRef} from "react";

const TodoInput = ({CreateTodo}) => {
    const todoInput = useRef('');

    const handleSubmit = (e) =>{
        e.preventDefault();
       CreateTodo(todoInput.current.value);
       e.target.value = "";
        todoInput.current.value="";
    }
    return(
        <div className="container border-dark d-flex justify-content-center">
            <form onSubmit={handleSubmit}>
                <input type="text" className="m-2" ref={todoInput}/>
                <input type="submit" value="+" className="btn m-2 btn-info" style={{borderRadius:4}}/>
            </form>
        </div>
    )
}

export default TodoInput;