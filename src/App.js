import React, {useState, useEffect} from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Tabs, Tab} from "react-bootstrap";
import './App.css';
import Todos from './components/Todos';
import Preloader from "./components/preloader";
import NavBar from "./components/Navbar";
import TodoInput from "./components/TodoInput";

function App() {
    const [todos, setTodos] = useState(null);

    useEffect(() => {
        const getTodos = async ()=>{
            const res = await axios.get("http://localhost:5000");
            setTodos(res.data);
        }
        getTodos();
    }, []);

    const CreateTodo = async (text) => {
        const res = await axios.post("http://localhost:5000",{message: text});
        setTodos(res.data);
    };

    //for tab control
    const [key, setKey] = useState('home');

  return <div className="App">
      <NavBar />
      <div className="container align-items-center">
          <div className="d-flex justify-content-center mt-5">
            <div className="card shadow">
                <div className="card-body">
                    <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)} >
                        <Tab eventKey="home" title="Home">
                            <div className="pre-scrollable">
                                {todos ? <Todos todos={todos} /> : <Preloader />}
                            </div>
                            <TodoInput CreateTodo={CreateTodo}/>
                        </Tab>
                        <Tab eventKey="profile" title="Done">
                            <h2>done items will shown here</h2>
                        </Tab>
                    </Tabs>
                </div>
            </div>
          </div>
      </div>
  </div>
}

export default App;
