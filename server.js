const express = require('express');
const {v4: uuid4} = require('uuid');
const  app = express();
const cors = require('cors');
app.use(cors());

//middleware for dta post request
app.use(express.json({extended: false}));

const todo = [
    {
        message: "hey man",
        id: 1,
        isDone: false
    },
    {
        message: "whatsup",
        id: 2,
        isDone: true,
    }
]

app.get("/",(req,res)=>{
    res.status(200).json(todo);
})

app.post("/",(req,res)=>{
    const Newtodo={
        message:req.body.message,
        id:uuid4()
    };
    todo.push(Newtodo);
    res.status(201).json(todo);
})

const PORT = 5000;
app.listen(PORT,()=>{
    console.log("server started");
})